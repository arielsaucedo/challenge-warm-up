import React from "react";
import { Switch, Route, Redirect } from "react-router";

import { HomeScreen } from "../components/home/HomeScreen";
import { NewPost } from "../components/new/NewPost";
import { PostDetails } from "../components/post/PostDetails";
import { EditPost } from "../components/edit/EditPost";
import { Navbar } from "../components/ui/Navbar";

/* Dashboard de las distintas rutas privadas */

export const DashboardRoutes = () => {
  return (
    <>
      <Navbar />
      <div>
        <Switch>
          <Route exact path="/posts" component={HomeScreen} />
          <Route exact path="/posts/new" component={NewPost} />
          <Route exact path="/posts/edit/:id" component={EditPost} />
          <Route exact path="/posts/:id" component={PostDetails} />

          <Redirect to="/posts" />
        </Switch>
      </div>
    </>
  );
};
