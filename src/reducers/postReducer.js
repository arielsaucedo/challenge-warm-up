/* Reducer de los post */
import { types } from "../types/types";

const initialState = {
  posts: [],
  active: [],
};

export const postReducer = (state = initialState, action) => {
  switch (action.type) {
    case types.getPosts:
      return {
        ...state,
        posts: [...action.payload],
      };

    case types.createPost:
      return {
        ...state,
        posts: [...state.posts, ...action.payload],
      };

    case types.postActive:
      return {
        ...state,
        active: [...action.payload],
      };

    case types.postClearActive:
      return {
        active: [],
      };

    case types.editPost:
      return {
        ...state,
        posts: state.posts.map((p) =>
          p.id === action.payload.id ? action.payload : p
        ),
      };

    case types.deletePost:
      return {
        ...state,
        posts: state.posts.filter((post) => post.id !== action.payload),
      };

    default:
      return state;
  }
};
