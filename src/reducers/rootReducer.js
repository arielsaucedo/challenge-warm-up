/* Reducer principal que recopila los reducer restantes */

import { combineReducers } from "redux";

import { authReducer } from "./authReducer";
import { postReducer } from "./postReducer";

export const rootReducer = combineReducers({
  auth: authReducer,
  results: postReducer,
});
