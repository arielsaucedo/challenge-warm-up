/* Tipos de acciones de los Reducers */

export const types = {
  authLogin: "[Auth] Login",
  authStartLogin: "[Auth] Start login",
  authLogout: "[Auth] Logout",

  getPosts: "[Post] Get Posts",
  createPost: "[Post] Create Post",
  postActive: "[Post] Set Post Active",
  postClearActive: "[Post] Clear Post Active",
  editPost: "[Post] Edit Post",
  deletePost: "[Post] Delete Post",
};
