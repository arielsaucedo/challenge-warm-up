import React, { useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";
import { getPosts, postClearActive } from "../../actions/post";

import { PostCard } from "../post/PostCard";

import "../../styles/homescreen.css";

export const HomeScreen = () => {
  const dispatch = useDispatch();
  const { posts } = useSelector((state) => state.results);

  useEffect(() => {
    dispatch(getPosts());
    dispatch(postClearActive());
  }, [dispatch]);

  if (posts === undefined) {
    return (
      <div className="text-center" style={{ color: "white" }}>
        <h3>Cargando...</h3>
      </div>
    );
  }

  return (
    <div className="d-flex flex-column justify-content-center homescreen">
      <h2
        className="text-center"
        style={{ color: "white", marginTop: 20, marginBottom: 20 }}
      >
        Listado de Posts
      </h2>
      <div className="d-flex flex-wrap justify-content-center">
        {posts.map((post) => (
          <div className="m-1" key={post.id}>
            <PostCard {...post} />
          </div>
        ))}
      </div>
    </div>
  );
};
