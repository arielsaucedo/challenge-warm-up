import React from "react";
import { Link } from "react-router-dom";
import { useDispatch } from "react-redux";
import { startPostDelete, postSetActive } from "../../actions/post";

export const PostCard = ({ id, title, body, userId }) => {
  const dispatch = useDispatch();

  const post = [
    {
      id: id,
      title: title,
      body: body,
      userId: userId,
    },
  ];

  const handlePostDelete = () => {
    dispatch(startPostDelete(id));
  };

  const setPostActive = () => {
    dispatch(postSetActive(post));
  };

  return (
    <div className="card" style={{ width: 300, height: 200, borderRadius: 20 }}>
      <div className="card-body d-flex flex-column align-content-center">
        <h5 className="card-title text-center">Titulo:</h5>
        <p className="card-title text-center">{title}</p>

        <div
          className="d-flex justify-content-around"
          style={{ position: "absolute", top: 150, left: 50 }}
        >
          <Link to={`/posts/${id}`}>
            <button className="btn btn-info btn-sm m-1" onClick={setPostActive}>
              Detalle
            </button>
          </Link>
          <Link to={`/posts/edit/${id}`}>
            <button
              className="btn btn-warning btn-sm m-1"
              onClick={setPostActive}
            >
              Editar
            </button>
          </Link>
          <button
            className="btn btn-danger btn-sm m-1"
            onClick={handlePostDelete}
          >
            Borrar
          </button>
        </div>
      </div>
    </div>
  );
};
