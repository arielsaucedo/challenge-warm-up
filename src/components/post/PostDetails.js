import React from "react";
import { useSelector } from "react-redux";
import { Link } from "react-router-dom";

export const PostDetails = () => {
  const { active } = useSelector((state) => state.results);
  const { title, body } = active[0];

  if (active === []) {
    return (
      <div>
        <h2>Esta intentando acceder a un post que no existe</h2>
      </div>
    );
  }

  return (
    <div className="d-flex justify-content-center mt-5">
      <div
        className="card"
        style={{ width: 400, height: 300, borderRadius: 20 }}
      >
        <div className="card-body d-flex flex-column align-content-center">
          <h5 className="card-title text-center">{title}</h5>
          <p className="card-text">{body}</p>
        </div>
        <div className="d-flex justify-content-center mb-4">
          <Link to="/posts">
            <button className="btn btn-primary">Volver</button>
          </Link>
        </div>
      </div>
    </div>
  );
};
