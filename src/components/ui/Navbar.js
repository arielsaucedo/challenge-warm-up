import React from "react";
import { useDispatch } from "react-redux";
import { NavLink } from "react-router-dom";
import { startLogout } from "../../actions/auth";

export const Navbar = () => {
  const dispatch = useDispatch();

  const handleLogout = () => {
    dispatch(startLogout());
  };

  return (
    <nav className="navbar navbar-expand-lg navbar-light bg-light">
      <div className="container-fluid">
        <button
          className="navbar-toggler navbar-toggler-button"
          type="button"
          data-bs-toggle="collapse"
          data-bs-target="#navbarWarmUp"
          aria-controls="navbarWarmUp"
          aria-expanded="false"
          aria-label="Toggle navigation"
        >
          <i className="navbar-toggler-icon"></i>
        </button>
        <img
          className="imagen"
          src="https://img.icons8.com/emoji/452/fire.png"
          height="40"
          alt=""
        />

        <div className="collapse navbar-collapse" id="navbarWarmUp">
          <ul className="navbar-nav me-auto mb-2 mb-lg-0">
            <li className="nav-item">
              <NavLink
                activeClassName="active"
                className="nav-item nav-link"
                exact
                to="/"
              >
                Home
              </NavLink>
            </li>
            <li className="nav-item">
              <NavLink
                activeClassName="active"
                className="nav-item nav-link"
                exact
                to="/posts/new"
              >
                Nuevo post
              </NavLink>
            </li>
          </ul>
          <div className="d-flex align-items-center">
            <h6>Logout</h6>
            <button className="btn" onClick={handleLogout}>
              <i className="fas fa-sign-out-alt" style={{ fontSize: 35 }}></i>
            </button>
          </div>
        </div>
      </div>
    </nav>
  );
};
