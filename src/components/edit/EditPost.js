import React from "react";

import { useParams } from "react-router-dom";
import { useDispatch, useSelector } from "react-redux";

import { Formik } from "formik";
import Swal from "sweetalert2";

import { editPost } from "../../actions/post";

import "../../styles/editPost.css";

export const EditPost = () => {
  const dispatch = useDispatch();

  const { id } = useParams();

  const { active } = useSelector((state) => state.results);
  const { title, body, userId } = active[0];

  if (active === []) {
    return (
      <div>
        <h2>Esta intentando editar un post que no existe</h2>
      </div>
    );
  }

  return (
    <div className="d-flex justify-content-center">
      <Formik
        initialValues={{ title: title, body: body }}
        validate={(values) => {
          const errors = {};
          if (!values.title) {
            errors.title = "Este campo es requerido";
          } else if (!values.title.length > 3) {
            errors.title = "El titulo debe tener al menos 3 caracteres";
          }

          if (!values.body) {
            errors.body = "Este campo es requerido";
          } else if (!values.body.length > 3) {
            errors.body = "El contenido debe tener al menos 3 caracteres";
          }
          return errors;
        }}
        onSubmit={(values) => {
          return dispatch(
            editPost(values.title, values.body, id, userId)
          ).catch(() => {
            Swal.fire({
              icon: "error",
              title: "Error!",
              text: "No se puede editar tu post",
            });
          });
        }}
      >
        {({
          values,
          touched,
          errors,
          handleChange,
          handleSubmit,
          isSubmitting,
        }) => (
          <form className="boxEdit" onSubmit={handleSubmit}>
            <h3>EDITAR POST</h3>
            <label>Titulo:</label>
            <input
              data-testid="title"
              autoComplete="off"
              type="text"
              name="title"
              placeholder={title}
              value={values.title}
              onChange={handleChange}
            />
            <div style={{ color: "red" }}>
              {errors.title && touched.title && errors.title}
            </div>

            <label>Contenido:</label>
            <textarea
              data-testid="body"
              type="text"
              name="body"
              placeholder={body}
              value={values.body}
              onChange={handleChange}
              rows="6"
            />
            <div style={{ color: "red" }}>
              {errors.body && touched.body && errors.body}
            </div>

            <input
              data-testid="button"
              className="btn-login"
              type="submit"
              id="edit"
              value="Guardar"
              disabled={isSubmitting}
            />
          </form>
        )}
      </Formik>
    </div>
  );
};
