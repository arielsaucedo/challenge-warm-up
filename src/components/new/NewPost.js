import React from "react";

import { Formik } from "formik";
import Swal from "sweetalert2";

import { useDispatch } from "react-redux";
import { createPost } from "../../actions/post";

import "../../styles/newPost.css";

export const NewPost = () => {
  const dispatch = useDispatch();

  return (
    <div className="d-flex justify-content-center">
      <Formik
        initialValues={{ title: "", body: "" }}
        validate={(values) => {
          const errors = {};
          if (!values.title) {
            errors.title = "Este campo es requerido";
          } else if (!values.title.length > 3) {
            errors.title = "El titulo debe tener al menos 3 caracteres";
          }

          if (!values.body) {
            errors.body = "Este campo es requerido";
          } else if (!values.body.length > 3) {
            errors.body = "El contenido debe tener al menos 3 caracteres";
          }
          return errors;
        }}
        onSubmit={(values) => {
          return dispatch(createPost(values.title, values.body)).catch(() => {
            Swal.fire({
              icon: "error",
              title: "Error!",
              text: "No se puede crear tu post",
            });
          });
        }}
      >
        {({
          values,
          touched,
          errors,
          handleChange,
          handleSubmit,
          isSubmitting,
        }) => (
          <form className="boxNew" onSubmit={handleSubmit}>
            <h3>CREAR NUEVO POST</h3>

            <label>Titulo:</label>
            <input
              id="title-new"
              data-testid="title"
              autoComplete="off"
              type="text"
              name="title"
              placeholder="Ingrese el titulo"
              value={values.title}
              onChange={handleChange}
            />
            <div style={{ color: "red" }}>
              {errors.title && touched.title && errors.title}
            </div>

            <label>Contenido:</label>
            <textarea
              data-testid="body"
              type="text"
              name="body"
              placeholder="Ingrese su posteo..."
              value={values.body}
              onChange={handleChange}
              rows="6"
            />
            <div style={{ color: "red" }}>
              {errors.body && touched.body && errors.body}
            </div>
            <input
              data-testid="button"
              className="btn-login"
              type="submit"
              id="create"
              value="Crear"
              disabled={isSubmitting}
            />
          </form>
        )}
      </Formik>
    </div>
  );
};
