import { postAPI } from "../api/postAPI";
import { types } from "../types/types";

/* Accion: obtener todos los post desde la API */
export const getPosts = () => {
  return async (dispatch) => {
    const resp = await postAPI.get(
      "https://jsonplaceholder.typicode.com/posts"
    );
    dispatch(allPosts(resp.data));
  };
};

const allPosts = (posts) => {
  return {
    type: types.getPosts,
    payload: posts,
  };
};

/* Accion: creacion de un nuevo post */
export const createPost = (title, body) => {
  return async (dispatch) => {
    const resp = await postAPI.post(
      "https://jsonplaceholder.typicode.com/posts",
      { title, body }
    );
    const nPost = [resp.data];
    nPost[0].userId = 10;
    console.log(nPost);

    dispatch(newPost(nPost));
  };
};

const newPost = (nPost) => {
  return {
    type: types.createPost,
    payload: nPost,
  };
};

/* Accion: seleccion de un post determinado */
export const postSetActive = (post) => {
  return {
    type: types.postActive,
    payload: post,
  };
};

export const postClearActive = () => {
  return {
    type: types.postClearActive,
  };
};

/* Accion: edicion de un post determinado */
export const editPost = (title, body, id, userId) => {
  return async (dispatch) => {
    const resp = await postAPI.put(
      `https://jsonplaceholder.typicode.com/posts/${id}`,
      {
        title,
        body,
        userId,
      }
    );
    const ePost = resp.data;
    console.log(ePost);

    dispatch(edPost(ePost));
  };
};

const edPost = (ePost) => {
  return {
    type: types.editPost,
    payload: ePost,
  };
};

/* Accion: borrado de un post */
export const startPostDelete = (id) => {
  return async (dispatch) => {
    await postAPI.delete(`https://jsonplaceholder.typicode.com/posts/${id}`);
    dispatch(deletePost(id));
  };
};

const deletePost = (id) => {
  return {
    type: types.deletePost,
    payload: id,
  };
};
